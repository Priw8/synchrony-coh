local Components = require "necro.game.data.Components"
local Event = require "necro.event.Event"
local CommonSpell = require "necro.game.data.spell.CommonSpell"
local Spell = require "necro.game.spell.Spell"
local Swipe = require "necro.game.system.Swipe"
local CustomEntities = require "necro.game.data.CustomEntities"
local Object = require "necro.game.object.Object"
local Attack = require "necro.game.character.Attack"
local Action = require "necro.game.system.Action"
local Inventory = require "necro.game.item.Inventory"
local Damage = require "necro.game.system.Damage"
local GrooveChain = require "necro.game.character.GrooveChain"
local ecs = require "system.game.Entities"
local SpellTargeting = require "necro.game.spell.SpellTargeting"
local Move = require "necro.game.system.Move"

Components.register{
    SpinAttackCastComponent = {
        Components.field.int8("staminaRequired"),
        Components.field.string("chargeSwipeType"),
        Components.field.string("swipeType")
    }
}

Components.register{
    SpinAttackCasterComponent = {
        Components.field.bool("isActive"),
        Components.field.bool("attackIsSpinAttack"),
        Components.field.string("chargeSwipeType"),
        Components.field.int32("effectiveDamage"),
        Components.field.bool("attackIsDamageCheck")
    },

    CoHchars_spellcastKnockbackLate = {
        Components.field.int("distance")
    }
}

Event.spellcast.add(
    "SpinAttack",
    {
        order = "casterTarget",
        filter = "CoHchars_SpinAttackCastComponent"
    },
    function (ev)
        if (ev.caster:hasComponent("CoHchars_SpinAttackCasterComponent")) then
            local spinCaster = ev.caster.CoHchars_SpinAttackCasterComponent
            local spinCast = ev.entity.CoHchars_SpinAttackCastComponent
            if (not spinCaster.isActive) then
                -- Prep spin attack - need to return the stamina to allow another cast
                local stamina = ev.caster.CoHchars_PlayerStaminaComponent
                stamina.currentStamina = stamina.currentStamina + spinCast.staminaRequired
                spinCaster.isActive = true
                spinCaster.chargeSwipeType = spinCast.chargeSwipeType
            else
                local dmg = Damage.getBaseDamage(ev.caster)
                spinCaster.effectiveDamage = dmg

                spinCaster.isActive = false

                spinCaster.attackIsSpinAttack = true
                Spell.cast(ev.caster, "CoHchars_SpellcastSpinAttackExecute", 0)
                spinCaster.attackIsSpinAttack = false
                Swipe.create {
                    attacker = ev.caster,
                    type = spinCast.swipeType,
                    x = ev.caster.position.x,
                    y = ev.caster.position.y,
                    direction = 0
                }
            end
        end
    end
)

-- Spin attack damage-dealing spellcast
CommonSpell.registerSpell("SpellcastSpinAttackExecute", {
    spellcastRectangular = {
        width = 3,
        height = 3,
        offsetX = -1,
        offsetY = -1
    },
    spellcastInflictDamage = {
        damage = 0 -- Irrelevant, gets overwritten in an event
    },
    -- Needs a custom component, for the knockback to happen afrer the damage
    CoHchars_spellcastKnockbackLate = {
        distance = 1
    }
})

Event.objectDealDamage.add(
    "SpinAttackDamage",
    {
        order = "applyDamage",
        sequence = -1,
        filter = "CoHchars_SpinAttackCasterComponent"
    },
    function (ev)
        local spinCaster = ev.entity.CoHchars_SpinAttackCasterComponent
        if (spinCaster.attackIsDamageCheck) then
            ev.suppressed = true
            spinCaster.effectiveDamage = ev.damage
        elseif (spinCaster.attackIsSpinAttack) then
            if (spinCaster.effectiveDamage ~= 0) then
                ev.damage = spinCaster.effectiveDamage + 1
            else
                ev.damage = 0
            end
        end
    end
)

-- Prevent normal attack when spin attack is prepared
Event.objectCheckAttack.add(
    "SpinAttackSuppressNormalAttack",
    {
        order = "dwarfism",
        filter = "CoHchars_SpinAttackCasterComponent"
    },
    function (ev)
        local spinCaster = ev.entity.CoHchars_SpinAttackCasterComponent
        if (spinCaster.isActive) then
            ev.suppressed = true
        end
    end
)

-- Prevent digging when spin attack is prepared
Event.objectDig.add(
    "SpinAttackSuppressNormalAttack",
    {
        order = "computeStrength",
        sequence = 1,
        filter = "CoHchars_SpinAttackCasterComponent"
    },
    function (ev)
        local spinCaster = ev.entity.CoHchars_SpinAttackCasterComponent
        if (spinCaster.isActive) then
            ev.strength = 0
        end
    end
)

Event.turn.add(
    "SpinAttackChargeSwipeCheck",
    {
        order = "playerActions",
        sequence = 1
    },
    function (ev)
        for entity in ecs.entitiesWithComponents{"CoHchars_SpinAttackCasterComponent"} do
            local spinCaster = entity.CoHchars_SpinAttackCasterComponent
            if (entity.character.canAct and spinCaster.isActive) then
                Swipe.create {
                    attacker = entity,
                    type = spinCaster.chargeSwipeType,
                    x = entity.position.x,
                    y = entity.position.y,
                    direction = 0
                }
            end
        end
    end
)

Event.spellcast.add("lateKnockback",
    {order = "inflictDamage", sequence = 1, filter = "CoHchars_spellcastKnockbackLate"},
    function (ev)
        local c = ev.entity.CoHchars_spellcastKnockbackLate
        for target in SpellTargeting.attackableTargets(ev, Attack.Flag.CHARACTER) do
            if target.knockbackable then
                local direction = Action.move(target.position.x - ev.x, target.position.y - ev.y)
                Damage.knockback(target, direction, c.distance, 
                    Move.Flag.mask(Move.Type.KNOCKBACK, Move.Flag.VOCALIZE, Move.Flag.TWEEN_HOP_FAIL), 1)
            end
        end
end)
