local Event = require "necro.event.Event"
local Components = require "necro.game.data.Components"
local ecs = require "system.game.Entities"
local HUD = require "necro.render.hud.HUD"
local GrooveChain = require "necro.game.character.GrooveChain"

-- Player stamina component
Components.register {
    PlayerStaminaComponent = {
        Components.field.int8("currentStamina"),
        Components.constant.int8("maxStamina")
    }
}

-- Draw stamina
Event.renderPlayerHUD.add(
    "renderStaminaGauge",
    {
        order = "equipment"
    },
    function(ev) 
        if (ev:hasComponent("CoHchars_PlayerStaminaComponent")) then
            local stamina = ev.CoHchars_PlayerStaminaComponent
            HUD.drawSprite {
                image = "mods/CoHchars/gfx/gui/hud_stamina_back.png",
                element = "spells",
                slot = {0.6, 0.75},
                imageRect = {0, 0, 50, 8},
                rect = {0, 0, 1.5, 0.2}
            }
            HUD.drawSprite {
                image = "mods/CoHchars/gfx/gui/hud_stamina_front.png",
                element = "spells",
                slot = {0.6, 0.75},
                imageRect = {0, 0, 50, 8},
                rect = {0, 0, (stamina.currentStamina / stamina.maxStamina) * 1.5, 0.2}
            }
        end
    end
)

-- Item stamina requirement component
Components.register {
    StaminaRequirementComponent = {
        Components.field.int8("staminaRequired")
    }
}

-- Check for the stamina requirement when using a spell
Event.spellItemCooldown.add(
    "stamina",
    {
        order = "kills",
        filter = "CoHchars_StaminaRequirementComponent"
    }, 
    function(ev)
        local holder = ecs.getEntityByID(ev.entity.item.holder)
        local currentStamina = 0
        if holder and holder:hasComponent("CoHchars_PlayerStaminaComponent") then
            currentStamina = holder.CoHchars_PlayerStaminaComponent.currentStamina
        end
        if holder then
            local difference = ev.entity.CoHchars_StaminaRequirementComponent.staminaRequired - currentStamina
            if difference > 0 then
                ev.cooldowns[#ev.cooldowns + 1] = {
                    name = "stamina",
                    pluralName = "stamina",
                    amount = difference,
                }
            end
        end
    end
)

-- Decrease stamina when using a spell
Event.spellItemActivate.add(
    "decreaseStamina",
    {
        order = "cast",
        filter = "CoHchars_StaminaRequirementComponent"
    },
    function(ev) 
        local holder = ecs.getEntityByID(ev.entity.item.holder)
        -- Technically, holder without the component should not be ever able to even use the spell
        -- because the cooldown would prevent it. But who knows what might happen..
        if holder and holder:hasComponent("CoHchars_PlayerStaminaComponent") then
           holder.CoHchars_PlayerStaminaComponent.currentStamina = holder.CoHchars_PlayerStaminaComponent.currentStamina - ev.entity.CoHchars_StaminaRequirementComponent.staminaRequired
        else
            dbg("Entity with no stamina component managed to use a spell with stamina requirement. How? Who knows...")
        end
    end
)

-- Refill stamina when killing an object
Event.objectKill.add(
    "refillStamina",
    {
        order = "grooveChain",
        -- Since the stamina refill uses the groove chain to determine the stamina restored,
        -- this handler must run before the grooveChain handler.
        sequence = -1,
        filter = "CoHchars_PlayerStaminaComponent"
    },
    function(ev)
        if (ev.credit) then
            local stamina = ev.entity.CoHchars_PlayerStaminaComponent
            -- Only restore stamina on kills that also affect stuff like kill cooldowns
            stamina.currentStamina = stamina.currentStamina + GrooveChain.getMultiplier(ev.entity) * 5
            
            if (stamina.currentStamina > stamina.maxStamina) then
                stamina.currentStamina = stamina.maxStamina
            end
        end
    end
)

