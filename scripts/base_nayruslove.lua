local Components = require "necro.game.data.Components"
local Event = require "necro.event.Event"
local Flyaway = require "necro.game.system.Flyaway"
local Invincibility = require "necro.game.character.Invincibility"
local Swipe = require "necro.game.system.Swipe"
local Damage = require "necro.game.system.Damage"
local ecs = require "system.game.Entities"

-- Used for the spellcast event filter
Components.register{ NayrusLoveCastComponent = {
    Components.constant.int8("maxDamage"),
    Components.constant.int8("reflectorDuration"),
    Components.constant.int8("invincibilityDuration"),
    Components.constant.string("swipeType")
} }

-- Used by the player to store whether Nayru's love is active or not
Components.register{
    NayrusLoveCasterComponent = {
        Components.field.int8("maxDamage"),
        Components.field.int8("durationLeft")
    }
}

Event.spellcast.add("NayrusLove",
    {
        order = "casterTarget",
        filter = "CoHchars_NayrusLoveCastComponent"
    },
    function(ev) 
        if (ev.caster:hasComponent("CoHchars_NayrusLoveCasterComponent")) then
            local nayru = ev.caster.CoHchars_NayrusLoveCasterComponent
            local nayruCast = ev.entity.CoHchars_NayrusLoveCastComponent

            nayru.maxDamage = nayruCast.maxDamage
            nayru.durationLeft = nayruCast.reflectorDuration
            Invincibility.activate(ev.caster, nayruCast.invincibilityDuration)

            -- Create the Nayru's love crystal "swipe"
            Swipe.create {
                attacker = ev.entity,
                type = nayruCast.swipeType,
                x = ev.caster.position.x,
                y = ev.caster.position.y,
                direction = 0
            }
        end
    end
)

-- Reflector handler
Event.objectTakeDamage.add(
    "NayrusLoveReflector",
    {
        order = "invincibility",
        sequence = -1,
        filter = "CoHchars_NayrusLoveCasterComponent"
    }, 
    function(ev)
        local nayru = ev.entity.CoHchars_NayrusLoveCasterComponent
        if (nayru.durationLeft > 0) then
            if (ev.damage > nayru.maxDamage) then
                ev.damage = nayru.maxDamage
            end

            -- Recursion check: we don't want 2 zeldas crashing the game by reflecting damage again and again
            -- Also, attacker can be nil sometimes (like hot coal damage)
            if (ev.attacker ~= nil and (not ev.attacker:hasComponent("CoHchars_NayrusLoveCasterComponent") or not (ev.attacker.CoHchars_NayrusLoveCasterComponent.durationLeft > 0))) then
                Damage.inflict {
                    attacker = ev.entity,
                    victim = ev.attacker,
                    damage = ev.damage,
                    type = Damage.Type.MAGIC -- Prevents damage ups such as ring of might from being applied.
                }
            end
            ev.damage = 0

            Flyaway.create {
                text = "Reflected!",
                entity = ev.entity
            }
        end
    end
)

Event.turn.add(
    "NayrusLoveReflector",
    {
        order = "playerActions",
        sequence = -1
    },
    function (ev)
        for entity in ecs.entitiesWithComponents{"CoHchars_NayrusLoveCasterComponent"} do
            local nayru = entity.CoHchars_NayrusLoveCasterComponent
            if (entity.character.canAct and nayru.durationLeft > 0) then
                nayru.durationLeft = nayru.durationLeft - 1
            end
        end
    end
)
