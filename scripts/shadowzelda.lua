local CustomEntities = require "necro.game.data.CustomEntities"
local Character = require "necro.game.character.Character"

local CommonSpell = require "necro.game.data.spell.CommonSpell"
local Event = require "necro.event.Event"
local Swipe = require "necro.game.system.Swipe"
local Collision = require "necro.game.tile.Collision"
local Team = require "necro.game.character.Team"
local AI = require "necro.game.enemy.ai.AI"
local Spell = require "necro.game.spell.Spell"
local Attack = require "necro.game.character.Attack"
local MoveAnimations = require "necro.render.level.MoveAnimations"


CustomEntities.extend {
    name = "CharacterShadowZelda",
    template = CustomEntities.template.player(0), -- Use Cadence as base
    
    components = {
        {
            friendlyName = {
                name = "Shadow Zelda"
            },
            textCharacterSelectionMessage = {
                text = "Shadow Zelda mode!\nUse powerful new spells.\nKill enemies to regenerate stamina."
            },
            sprite = {
                texture = "mods/CoHchars/gfx/entities/char_shadowzelda_armor_body.png"
            },
            bestiary = {
                image = "mods/CoHchars/gfx/bestiary/bestiary_shadowzelda.png",
                focusX = 150,
                focusY = 115
            },
            initialInventory = {
                items = {
                    "WeaponDagger",
                    "ShovelBasic",
                    "Torch1", -- Basic torch
                    "Bomb",

                    -- Zelda's spells
                    "CoHchars_SpellShadowNayrusLove",
                    "CoHchars_SpellShadowDinsFire"
                }
            },
            CoHchars_PlayerStaminaComponent = {
                currentStamina = 100,
                maxStamina = 100
            },
            CoHchars_NayrusLoveCasterComponent = {
                maxDamage = 0,
                durationLeft = 0
            },
            CoHchars_DinsFireCasterComponent = {
                entityId = 0
            }
        },
        {
            sprite = {
                texture = "mods/CoHchars/gfx/entities/char_shadowzelda_heads.png"
            }
        }
    }
}



-- Nayru's love things

-- The spell item
CustomEntities.extend {
    name = "SpellShadowNayrusLove",
    template = CustomEntities.template.item(),

    data = {
        flyaway = "Shadow Nayru's Love",
        hint = "Reflect attacks back at enemies",
        slot = "spell"
    },

    components = {
        sprite = {
            texture = "mods/CoHchars/gfx/items/spell_shadownayruslove.png"
        },
        itemCastOnUse = {
            spell = "CoHchars_SpellcastShadowNayrusLove"
        },
        CoHchars_StaminaRequirementComponent = {
            staminaRequired = 20
        }
    }
}

-- The spellcast itself
CommonSpell.registerSpell("SpellcastShadowNayrusLove", {
    spellcastTargetCaster = {},

    soundSpellcast = {
        sound = "spellGeneral",
    },
    friendlyName = {
        name = "Shadow Nayru's Love",
    },
    CoHchars_NayrusLoveCastComponent = {
        maxDamage = 1,
        reflectorDuration = 2,
        invincibilityDuration = 2,
        swipeType = "CoHchars_SwipeShadowNayrusLove"
    }
})

-- Add swipe effect stuff
Event.swipe.add("ShadowZeldaSpellShadowNayrusLove", "CoHchars_SwipeShadowNayrusLove", 
    function(ev)
        ev.entity.swipe.texture = "mods/CoHchars/gfx/swipes/swipe_shadownayruslove.png"
        ev.entity.swipe.frameCount = 16
        ev.entity.swipe.width = 24
        ev.entity.swipe.height = 32
        ev.entity.swipe.duration = 0.5
    end
)


-- Din's fire stuff
-- TODO: prevent din casts by entities with no DinsFireCaster, even if they do have PlayerStamina
-- (relevant for when I add other characters)

-- The spell item
CustomEntities.extend {
    name = "SpellShadowDinsFire",
    template = CustomEntities.template.item(),

    data = {
        flyaway = "Shadow Din's Fire",
        hint = "Explosive projectile",
        slot = "spell"
    },

    components = {
        sprite = {
            texture = "mods/CoHchars/gfx/items/spell_shadowdinsfire.png"
        },
        itemCastOnUse = {
            spell = "CoHchars_SpellcastShadowDinsFire"
        },
        CoHchars_StaminaRequirementComponent = {
            staminaRequired = 40
        },
        CoHchars_isDinsFire = {}
    }
}

-- The spellcast itself
CommonSpell.registerSpell("SpellcastShadowDinsFire", {
    spellcast = {},

    spellcastLinear = {
        collisionMask = Collision.Type.WALL,
        minDistance = 1,
        maxDistance = 1,
    },
    soundSpellcast = {
        sound = "spellGeneral",
    },
    friendlyName = {
        name = "Shadow Din's Fire",
    },

    CoHchars_DinsFireCastComponent = {
        playerControl = false,
        staminaRequired = 40,
        entityType = "CoHchars_ShadowDinsFireEntity",
        swipeType = "CoHchars_SwipeShadowDinsFire"
    }
})

-- Din's fire entity
CustomEntities.extend {
    name = "ShadowDinsFireEntity",
    template = CustomEntities.template.enemy(),

    components = {
        sprite = {
            texture = "mods/CoHchars/gfx/entities/entity_shadowdinsfire.png",
            width = 24,
            height = 24
        },
        shadow = {
            offsetY = 4
        },
        silhouette = {
            activeFrameY = 1
        },

        friendlyName = {
            name = "Shadow Din's Fire"
        },
        team = {
            id = Team.Id.ENEMY
        },
        dropCurrencyOnDeath = false,
        killCredit = false,
        ai = {
            id = AI.Type.LINEAR
        },
        facingDirection = {},
        priority = {
            value = 200000000
        },
        tween = {
            defaultTween = MoveAnimations.Type.SLIDE
        },
        sinkable = false,
        attackable = {
            flags = Attack.Flag.mask(Attack.Flag.DEFAULT, Attack.Flag.PROVOKE)
        },
        visionRadial = {
            active = true,
            radius = 768
        },
        lightSource = {},
        hasMoved = {},

        CoHchars_DinsFireEntityComponent = {
            casterId = 0,
            teamId = 0,
            staminaRequired = 40
        },
        innateAttack = false
    }
}

-- Din swipe handler...
Event.swipe.add("spellShadowDinsFire", "CoHchars_SwipeShadowDinsFire", 
    function(ev)
        ev.entity.swipe.texture = "mods/CoHchars/gfx/swipes/swipe_shadowdinsfire.png"
        ev.entity.swipe.frameCount = 10
        ev.entity.swipe.width = 16
        ev.entity.swipe.height = 24
        ev.entity.swipe.duration = 0.75
        ev.entity.swipe.offsetX = 4
    end
)
