local CustomEntities = require "necro.game.data.CustomEntities"
local Character = require "necro.game.character.Character"

local CommonSpell = require "necro.game.data.spell.CommonSpell"
local Event = require "necro.event.Event"
local Swipe = require "necro.game.system.Swipe"
local Collision = require "necro.game.tile.Collision"
local Team = require "necro.game.character.Team"
local AI = require "necro.game.enemy.ai.AI"
local Spell = require "necro.game.spell.Spell"
local Attack = require "necro.game.character.Attack"
local MoveAnimations = require "necro.render.level.MoveAnimations"


CustomEntities.extend {
    name = "CharacterZelda",
    template = CustomEntities.template.player(0), -- Use Cadence as base
    
    components = {
        {
            friendlyName = {
                name = "Zelda"
            },
            textCharacterSelectionMessage = {
                text = "Zelda mode!\nUse powerful new spells.\nKill enemies to regenerate stamina."
            },
            sprite = {
                texture = "mods/CoHchars/gfx/entities/char_zelda_armor_body.png"
            },
            bestiary = {
                image = "mods/CoHchars/gfx/bestiary/bestiary_zelda.png",
                focusX = 90,
                focusY = 115
            },
            initialInventory = {
                items = {
                    "WeaponDagger",
                    "ShovelBasic",
                    "Torch1", -- Basic torch
                    "Bomb",

                    -- Zelda's spells
                    "CoHchars_SpellNayrusLove",
                    "CoHchars_SpellDinsFire"
                }
            },
            CoHchars_PlayerStaminaComponent = {
                currentStamina = 100,
                maxStamina = 100
            },
            CoHchars_NayrusLoveCasterComponent = {
                maxDamage = 0,
                durationLeft = 0
            },
            CoHchars_DinsFireCasterComponent = {
                entityId = 0
            }
        },
        {
            sprite = {
                texture = "mods/CoHchars/gfx/entities/char_zelda_heads.png"
            }
        }
    }
}



-- Nayru's love things

-- The spell item
CustomEntities.extend {
    name = "SpellNayrusLove",
    template = CustomEntities.template.item(),

    data = {
        flyaway = "Nayru's Love",
        hint = "Reflect attacks back at enemies",
        slot = "spell"
    },

    components = {
        sprite = {
            texture = "mods/CoHchars/gfx/items/spell_nayruslove.png"
        },
        itemCastOnUse = {
            spell = "CoHchars_SpellcastNayrusLove"
        },
        CoHchars_StaminaRequirementComponent = {
            staminaRequired = 20
        }
    }
}

-- The spellcast itself
CommonSpell.registerSpell("SpellcastNayrusLove", {
    spellcastTargetCaster = {},

    soundSpellcast = {
        sound = "spellGeneral",
    },
    friendlyName = {
        name = "Nayru's Love",
    },
    CoHchars_NayrusLoveCastComponent = {
        maxDamage = 2,
        reflectorDuration = 1,
        invincibilityDuration = 2,
        swipeType = "CoHchars_SwipeNayrusLove"
    }
})

-- Add swipe effect stuff
Event.swipe.add("ZeldaSpellNayrusLove", "CoHchars_SwipeNayrusLove", 
    function(ev)
        ev.entity.swipe.texture = "mods/CoHchars/gfx/swipes/swipe_nayruslove.png"
        ev.entity.swipe.frameCount = 16
        ev.entity.swipe.width = 24
        ev.entity.swipe.height = 32
        ev.entity.swipe.duration = 0.5 -- in s
    end
)


-- Din's fire stuff
-- TODO: prevent din casts by entities with no DinsFireCaster, even if they do have PlayerStamina
-- (relevant for when I add other characters)

-- The spell item
CustomEntities.extend {
    name = "SpellDinsFire",
    template = CustomEntities.template.item(),

    data = {
        flyaway = "Din's Fire",
        hint = "Remote-controlled explosion",
        slot = "spell"
    },

    components = {
        sprite = {
            texture = "mods/CoHchars/gfx/items/spell_dinsfire.png"
        },
        itemCastOnUse = {
            spell = "CoHchars_SpellcastDinsFire"
        },
        CoHchars_StaminaRequirementComponent = {
            staminaRequired = 40
        },
        CoHchars_isDinsFire = {}
    }
}

-- The spellcast itself
CommonSpell.registerSpell("SpellcastDinsFire", {
    spellcast = {},

    spellcastLinear = {
        collisionMask = Collision.Type.WALL,
        minDistance = 1,
        maxDistance = 1,
    },
    soundSpellcast = {
        sound = "spellGeneral",
    },
    friendlyName = {
        name = "Din's Fire",
    },

    CoHchars_DinsFireCastComponent = {
        playerControl = true,
        staminaRequired = 40,
        entityType = "CoHchars_DinsFireEntity",
        swipeType = "CoHchars_SwipeDinsFire"
    }
})

-- Din's fire entity
CustomEntities.extend {
    name = "DinsFireEntity",
    template = CustomEntities.template.enemy(),

    components = {
        sprite = {
            texture = "mods/CoHchars/gfx/entities/entity_dinsfire.png",
            width = 24,
            height = 24
        },
        shadow = {
            offsetY = 4
        },
        silhouette = {
            activeFrameY = 1
        },

        friendlyName = {
            name = "Din's Fire"
        },
        team = {
            id = Team.Id.ENEMY
        },
        dropCurrencyOnDeath = false,
        killCredit = false,
        ai = {
            id = AI.Type.LINEAR
        },
        facingDirection = {},
        priority = {
            value = 200000000
        },
        tween = {
            defaultTween = MoveAnimations.Type.SLIDE
        },
        sinkable = false,
        attackable = {
            flags = Attack.Flag.mask(Attack.Flag.DEFAULT, Attack.Flag.PROVOKE)
        },
        visionRadial = {
            active = true,
            radius = 768
        },
        lightSource = {},
        hasMoved = {},

        CoHchars_DinsFireEntityComponent = {
            casterId = 0,
            teamId = 0,
            staminaRequired = 40
        },
        innateAttack = false
    }
}

-- Din swipe handler...
Event.swipe.add("spellDinsFire", "CoHchars_SwipeDinsFire", 
    function(ev)
        ev.entity.swipe.texture = "mods/CoHchars/gfx/swipes/swipe_dinsfire.png"
        ev.entity.swipe.frameCount = 10
        ev.entity.swipe.width = 16
        ev.entity.swipe.height = 24
        ev.entity.swipe.duration = 0.75 -- in s
        ev.entity.swipe.offsetX = 4
    end
)
