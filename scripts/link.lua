local CustomEntities = require "necro.game.data.CustomEntities"
local Character = require "necro.game.character.Character"
local CommonSpell = require "necro.game.data.spell.CommonSpell"
local Event = require "necro.event.Event"

CustomEntities.extend {
    name = "CharacterLink",
    template = CustomEntities.template.player(0), -- Use Cadence as base
    
    components = {
        {
            friendlyName = {
                name = "Link"
            },
            textCharacterSelectionMessage = {
                text = "Link mode!\nUnleash a powerful spin attack!\nKill enemies to regenerate stamina."
            },
            sprite = {
                texture = "mods/CoHchars/gfx/entities/char_link_armor_body.png"
            },
            bestiary = {
                image = "mods/CoHchars/gfx/bestiary/bestiary_link.png",
                focusX = 140,
                focusY = 95
            },
            initialInventory = {
                items = {
                    "WeaponDagger",
                    "ShovelBasic",
                    "Torch1", -- Basic torch
                    "Bomb",

                    "CoHchars_SpellSpinAttack"
                }
            },
            CoHchars_PlayerStaminaComponent = {
                currentStamina = 100,
                maxStamina = 100
            },
            CoHchars_SpinAttackCasterComponent = {
                isActive = false
            }
        },
        {
            sprite = {
                texture = "mods/CoHchars/gfx/entities/char_link_heads.png"
            }
        }
    }
}

-- Spin attack

-- The spell item
CustomEntities.extend {
    name = "SpellSpinAttack",
    template = CustomEntities.template.item(),

    data = {
        flyaway = "Spin Attack",
        hint = "PLACEHOLDER",
        slot = "spell"
    },

    components = {
        sprite = {
            texture = "mods/CoHchars/gfx/items/spell_spinattack.png"
        },
        itemCastOnUse = {
            spell = "CoHchars_SpellcastSpinAttack"
        },
        CoHchars_StaminaRequirementComponent = {
            staminaRequired = 25
        }
    }
}

-- The spellcast itself
CommonSpell.registerSpell("SpellcastSpinAttack", {
    spellcastTargetCaster = {},

    soundSpellcast = {
        sound = "spellGeneral",
    },
    friendlyName = {
        name = "Spin Attack",
    },
    CoHchars_SpinAttackCastComponent = {
        staminaRequired = 25,
        chargeSwipeType = "CoHchars_SwipeSpinAttackCharge",
        swipeType = "CoHchars_SwipeSpinAttack"
    }
})

Event.swipe.add("SpellSpinAttackCharge", "CoHchars_SwipeSpinAttackCharge", 
    function(ev)
        ev.entity.swipe.texture = "mods/CoHchars/gfx/swipes/swipe_spincharge.png"
        ev.entity.swipe.frameCount = 4
        ev.entity.swipe.width = 48
        ev.entity.swipe.height = 48
        ev.entity.swipe.offsetX = -12
        ev.entity.swipe.duration = 0.2
    end
)

Event.swipe.add("SpellSpinAttack", "CoHchars_SwipeSpinAttack", 
    function(ev)
        ev.entity.swipe.texture = "mods/CoHchars/gfx/swipes/swipe_spin.png"
        ev.entity.swipe.frameCount = 4
        ev.entity.swipe.width = 48
        ev.entity.swipe.height = 48
        ev.entity.swipe.offsetX = -24
        ev.entity.swipe.offsetY = 4
        ev.entity.swipe.scaleX = 1.5
        ev.entity.swipe.scaleY = 1.5
        ev.entity.swipe.duration = 0.2
    end
)

