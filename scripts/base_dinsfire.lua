local Components = require "necro.game.data.Components"
local Event = require "necro.event.Event"
local Swipe = require "necro.game.system.Swipe"
local Damage = require "necro.game.system.Damage"
local ecs = require "system.game.Entities"
local Object = require "necro.game.object.Object"
local Action = require "necro.game.system.Action"
local AI = require "necro.game.enemy.ai.AI"
local Team = require "necro.game.character.Team"
local Spell = require "necro.game.spell.Spell"
local Move = require "necro.game.system.Move"

-- Used by player to store current Din's fire information
Components.register {
    DinsFireCasterComponent = {
        Components.field.entityID("entityId"),
        Components.field.string("swipeType")
    }
}

-- Used by the spellcast
Components.register {
    DinsFireCastComponent = {
        Components.constant.bool("playerControl"),
        Components.constant.int8("staminaRequired"), -- to rollback stamina in case the cast is just an explode signal for existing din's fire
        Components.constant.string("entityType"),
        Components.constant.string("swipeType")
    }
}

-- Used by the Din's fire entity
Components.register { 
    DinsFireEntityComponent = {
        Components.field.entityID("casterId"),
        Components.field.int32("teamId"), -- team of the caster
        Components.field.int32("staminaRequired"),
        Components.field.string("swipeType"),
        Components.field.bool("hasExploded")
    }
}

-- Component to be used by the din's fire item 
-- (related to blocking usage of other spells when Din's fire is being casted - see the spellItemCooldown handler)
Components.register {
    isDinsFire = {}
}

Event.spellcast.add(
    "DinsFire",
    {
        order = "casterTarget",
        filter = "CoHchars_DinsFireCastComponent"
    },
    function(ev)
        if (ev.caster:hasComponent("CoHchars_DinsFireCasterComponent")) then
            local dinCaster = ev.caster.CoHchars_DinsFireCasterComponent
            local dinCast = ev.entity.CoHchars_DinsFireCastComponent
            dinCaster.swipeType = dinCast.swipeType

            -- Give caster the stamina back: it is subtracted when din's fire explodes.
            -- It has to be like that to allow remote detonation.
            -- It is assumed that the caster has the stamina component, since otherwise they wouldn't cast the spell.
            -- ...but only do that if Din's fire doesn't start in the detached state.
            if (dinCast.playerControl) then
                ev.caster.CoHchars_PlayerStaminaComponent.currentStamina = 
                    ev.caster.CoHchars_PlayerStaminaComponent.currentStamina + dinCast.staminaRequired
            end
    
            if (dinCaster.entityId == 0 or not ecs.entityExists(dinCaster.entityId)) then
                -- Din's fire does not exist - create new one
                local dinsFireEntity = Object.spawn(
                    dinCast.entityType,
                    ev.caster.position.x,
                    ev.caster.position.y,
                    {}
                )
                dinsFireEntity.CoHchars_DinsFireEntityComponent.teamId = ev.caster.team.id
                dinsFireEntity.CoHchars_DinsFireEntityComponent.swipeType = dinCast.swipeType
                dinsFireEntity.CoHchars_DinsFireEntityComponent.hasExploded = false

                -- Make Din's fire move where player is facing
                local castDir = ev.caster.facingDirection.direction

                -- Can be 0 if the caster didn't move before casting...
                -- We don't want it to be stuck not moving, so let's default to moving somewhere
                if (castDir == 0) then
                    castDir = 1
                end
                
                Move.direction(dinsFireEntity, castDir, 1, Move.Type.NORMAL)
                dinsFireEntity.facingDirection.direction = castDir

                if (dinCast.playerControl) then
                    -- Bind caster and the entity
                    dinsFireEntity.CoHchars_DinsFireEntityComponent.casterId = ev.caster.id
                    dinCaster.entityId = dinsFireEntity.id

                    -- Cool effects (tm)
                    createDinEff(ev.caster, dinCaster.swipeType, ev.caster.position.x, ev.caster.position.y)
                end
            else
                -- Din's fire does exist - detonate!
                local dinsFireEntity = ecs.getEntityByID(dinCaster.entityId)
                dinExplode(dinsFireEntity)
            end
        end        
    end
)

-- Lose control over Din's fire when receiving damage (detach)
Event.objectTakeDamage.add(
    "DinsFireInterrupt",
    {
        order = "death",
        sequence = 1,
        filter = "CoHchars_DinsFireCasterComponent"
    },
    function(ev)
        local dinCaster = ev.entity.CoHchars_DinsFireCasterComponent
        if (dinCaster.entityId ~= 0) then
            local dinEntity = ecs.getEntityByID(dinCaster.entityId)
            if (dinEntity ~= nil) then
                local din = dinEntity.CoHchars_DinsFireEntityComponent
                local stamina = ev.entity.CoHchars_PlayerStaminaComponent
                stamina.currentStamina = stamina.currentStamina - din.staminaRequired

                -- Should NOT happen, at least in theory
                if (stamina.currentStamina < 0) then
                    dbg("Din detach stamina failsafe")
                    stamina.currentStamina = 0
                end

                din.casterId = 0 -- detach from caster
            end
            dinCaster.entityId = 0
        end
    end
)

-- Prevent caster movement casting Din's fire
Event.objectDirection.add(
    "checkDinsFireCaster",
    {
        order = "hasMoved",
        sequence = -1,
        filter = "CoHchars_DinsFireCasterComponent"
    },
    function(ev)
        local dinCaster = ev.entity.CoHchars_DinsFireCasterComponent
        if (dinCaster.entityId ~= 0) then
            if (not ecs.entityExists(dinCaster.entityId)) then
                dinCaster.entityId = 0
            else
                ev.result = Action.Result.IDLE
                local dinEntity = ecs.getEntityByID(dinCaster.entityId)
                if (dinEntity.facingDirection.direction == 0) then
                    dinEntity.facingDirection.direction = ev.direction
                end
                createDinEff(ev.entity, dinCaster.swipeType, ev.entity.position.x, ev.entity.position.y)
            end
        end
    end
)

-- Apply caster direction to Din's fire (while the caster can't move, they can still rotate!)
Event.objectDirection.add(
    "applyDinsFireDirection",
    {
        order = "hasMoved",
        sequence = -1,
        filter = "CoHchars_DinsFireEntityComponent"
    },
    function(ev)
        ev.requeue = false
        local din = ev.entity.CoHchars_DinsFireEntityComponent
        if (din.casterId ~= 0 and ecs.entityExists(din.casterId)) then
            local caster = ecs.getEntityByID(din.casterId)
            ev.direction = caster.facingDirection.direction
            ev.entity.facingDirection.direction = ev.direction
            local offsetX, offsetY = Action.getMovementOffset(ev.direction)
            ev.dx = offsetX
            ev.dy = offsetY
        end
    end
)

-- Create Din's fire eff on the spell entity
Event.objectMove.add(
    "dinSwipe",
    {
        order = "hasMoved",
        sequence = 1,
        filter = "CoHchars_DinsFireEntityComponent"
    },
    function(ev)
        createDinEff(ev.entity, ev.entity.CoHchars_DinsFireEntityComponent.swipeType, ev.entity.position.x, ev.entity.position.y)
    end
)

Event.objectMoveResult.add(
    "dinExplode",
    {
        order = "spellcast",
        sequence = -1,
        filter = "CoHchars_DinsFireEntityComponent"
    },
    function(ev)
        if (ev.result ~= Action.Result.MOVE) then
            dinExplode(ev.entity)
        end
    end
)

Event.objectTakeDamage.add(
    "dinExplode",
    {
        order = "immunity",
        sequence = -1,
        filter = "CoHchars_DinsFireEntityComponent"
    },
    function(ev)
        dinExplode(ev.entity, ev.penetration == Damage.Penetration.PHASING)
    end
)

-- Explosion handler thing
function dinExplode(entity, dontActuallyExplode)
    local din = entity.CoHchars_DinsFireEntityComponent

    -- The point of this check is mostly to prevent infinite recursion
    -- How could this happen? Casting the explosion damages Din's fire and fires the
    -- objectTakeDamage handler, which then calls dinExplode again
    -- Kind of a weird thing... Originally I made it a SpellCastOnDeath, but that lead to some
    -- weird inconsistencies when the handler was fired without dinExplode executing first
    -- So in the end, I decided that making it call dinExplode on taking damage is better.
    -- Thus, adding the need for the recursion check.
    if (din.hasExploded) then
        return
    end

    din.hasExploded = true
    Team.setTeam(entity, din.teamId) -- for shopkeep aggro purposes

    -- Take stamina from the caster
    if (din.casterId ~= 0 and ecs.entityExists(din.casterId)) then
        local caster = ecs.getEntityByID(din.casterId)
        caster.CoHchars_DinsFireCasterComponent.entityId = 0
        local stamina = caster.CoHchars_PlayerStaminaComponent
        stamina.currentStamina = stamina.currentStamina - din.staminaRequired

        -- Technically SHOULD NOT happen
        if (stamina.currentStamina < 0) then
            dbg("dinExplode stamina failsafe")
            stamina.currentStamina = 0
        end
    end

    if (not dontActuallyExplode) then
        Spell.cast(
            entity,
            "SpellcastExplosion",
            0
        )
    end
end

-- Wrapper for creating the din effect
function createDinEff(entity, type, x, y)
    Swipe.create {
        attacker = entity,
        type = type,
        x = x,
        y = y,
        direction = 0
    }
end

-- "What the heck is this jank", you may ask
-- Basically, since Din's fire is made to consume stamina quite a bit *after* the spell use
-- any stamina-consuming spells should be blocked, and this is done by forcing a cooldown on them
-- if the holder is casting din's fire.
Event.spellItemCooldown.add(
    "dinsFireDontUseOtherStaminaSpells",
    {
        order = "kills",
        filter = "CoHchars_StaminaRequirementComponent"
    },
    function(ev)
        if (not ev.entity:hasComponent("CoHchars_isDinsFire")) then
            local holder = ecs.getEntityByID(ev.entity.item.holder)
            if (holder and holder:hasComponent("CoHchars_DinsFireCasterComponent") and holder.CoHchars_DinsFireCasterComponent.entityId ~= 0) then
                ev.cooldowns[#ev.cooldowns + 1] = {
                    name = "Din's Fire\ncurrently\nactive",
                    amount = 1
                }
            end
        end
    end
)

